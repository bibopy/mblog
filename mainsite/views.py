# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import redirect
from django.shortcuts import render
from datetime import datetime
from .models import Post



# Create your views here.

def homepage(request):
    posts = Post.objects.all()
    now=datetime.now()
    return render(request,'index.html',locals())

#先在views.py定義好showpost後引入urls.py 

def showpost(request, slug):
    try:
        post=Post.objects.get(slug = slug)
        if post != None:
            return render(request ,'post.html',locals())
    except:
        return redirect('/')






